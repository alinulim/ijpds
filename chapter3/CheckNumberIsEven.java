import java.util.Scanner;

public class CheckNumberIsEven {
    public static void main(String[] args){
        Scanner input = new Scanner (System.in);
        System.out.print("Enter a number: ");
        int number = input.nextInt();
        
        // Thhe following block can be replaced...

        /*if (number % 2 == 0)
            even = true;
        else
            even = false;
    }   */

        // with this.

        boolean even = number % 2 == 0;

        System.out.println(even);    
    }
}
